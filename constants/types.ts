export type ButtonProps = {
  title: string;
  variant: 'btn_dark_green';
  icon?: string;
  type: "submit" | "reset" | "button" | undefined;
};
