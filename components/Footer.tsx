import { FOOTER_CONTACT_INFO, FOOTER_LINKS, SOCIALS } from "@/constants";
import Image from "next/image";
import React from "react";

const Footer = () => {
  return (
    <div className="flex flexBetween max-container">
      <Image src="/hilink-logo.svg" alt="logo" height={29} width={74} />
      {FOOTER_LINKS.map((item) => (
        <ul>
          <label className="bold-16 whitespace-nowrap">{item.title}</label>
          <ul>
            {item.links.map((link) => (
              <li className="mt-3 cursor-pointer text-gray-30">{link}</li>
            ))}
          </ul>
        </ul>
      ))}

      <ul>
        <label className="bold-16 whitespace-nowrap">
          {FOOTER_CONTACT_INFO.title}
        </label>
        <ul>
          {FOOTER_CONTACT_INFO.links.map((link) => (
            <li className="mt-3 cursor-pointer text-gray-30">{link.label}</li>
          ))}
        </ul>
      </ul>

      <ul>
        <label className="bold-16 whitespace-nowrap">
          {SOCIALS.title}
        </label>
        <ul className="flexBetween gap-3">
          {SOCIALS.links.map((link) => (
            <Image src={link} alt="social" height={24} width={24} />
          ))}
        </ul>
      </ul>
    </div>
  );
};

export default Footer;
