import { ButtonProps } from "@/constants/types";
import Image from "next/image";

const Button = ({ type, title, icon, variant }: ButtonProps) => {
  return (
    <button className={`${variant} flexCenter gap-3 rounded-full border`} type={type}>
      {icon && <Image src={icon} alt={title} height={24} width={24} />}
      <label className="bold-16 whitespace-nowrap">{title}</label>
    </button>
  );
};

export default Button;
